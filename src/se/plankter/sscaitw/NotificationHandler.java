package se.plankter.sscaitw;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;

/**
 * Created by Plankton on 2014-12-25.
 */
public class NotificationHandler {

    private TrayIcon trayIcon;

    public NotificationHandler() {

        if (!SystemTray.isSupported()) {
            // Go directory to the task;
            return;
        }

        Image icon = createIcon("res" + File.separator + "tray_icon.gif", "Sample tray icon");
        if (icon == null) {
            // Go directory to the task;
            return;
        }

        // access the system tray. If not supported
        // or if notification area is not present (Ubuntu)
        // a NotSupportedException exception is trown;

        final SystemTray tray = SystemTray.getSystemTray();
        Dimension trayIconSize = tray.getTrayIconSize();

        // create the trayIcon itself.
        trayIcon = new TrayIcon(icon.getScaledInstance(trayIconSize.width, -1, Image.SCALE_SMOOTH));


        // Add tooltip and menu to trayicon
        //trayIcon.setToolTip("Tray icon demo");

        // Add the trayIcon to system tray/notification
        // area

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("Could not load tray icon !");
        }
    }

    protected static Image createIcon(String path, String description) {
        Image image = null;
        File imageFile = new File(path);
        try {
            image = ImageIO.read(imageFile);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        if (image == null) {
            System.err.println(imageFile.getAbsolutePath() + " not found");
            return null;
        } else {
            return (new ImageIcon(image, description)).getImage();
        }
    }


    public void showNotification(Matchup nextMatchup, String winner, String loser) {
        if (!SystemTray.isSupported()) {
            // Don't use system tray notifications
            return;
        }
        //System.out.println("Should display notification");

        String upcomingMatch = "";
        if (nextMatchup != null) {
            int matchNr = nextMatchup.getNumber();
            String tmpString = "";
            if (matchNr == 0) {
                tmpString = "is playing NOW!";
            } else if (matchNr == 1) {
                tmpString = "is playing in the next match.";
            } else {
                tmpString = "is playing in " + matchNr + " matches.";
            }
            upcomingMatch = "\n\nUpcoming match:\n" + nextMatchup.getPlayers() + "\n" + tmpString;
        }

        String lastResult = winner + " won the last match against " + loser;
        String notificationText = lastResult + upcomingMatch;
        trayIcon.displayMessage("Match result", notificationText, java.awt.TrayIcon.MessageType.INFO);
    }
}
